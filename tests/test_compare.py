"""Test cases for compare module."""

import unittest
from unittest import mock

from upt_compare.compare import UptCompare
from upt_compare.dumb_db import DumbDb


class TestConsole(unittest.TestCase):
    """Testcase for UptCompare."""

    @mock.patch('upt_compare.comments.get_env_var_or_raise', mock.Mock())
    @mock.patch('upt_compare.compare.get_env_var_or_raise', mock.Mock())
    @mock.patch('upt_compare.compare.CommentsHelper', mock.Mock())
    def test_stage2_run_tests(cls):
        """Ensure stage2_run_tests works."""
        dumb_db = DumbDb(set(), {3, 4}, {5, 6})
        upt_compare = UptCompare()

        with mock.patch.object(dumb_db, 'update') as mock_update:
            with mock.patch.object(upt_compare, 'run_comparison_test') as mock_run:
                mock_run.return_value = True

                upt_compare.stage2_run_tests(dumb_db)

                assert mock_run.call_count == 2
                assert mock_update.call_count == 1
