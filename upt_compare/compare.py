"""Module to compare results of upt and skt."""
import glob
import time
from datetime import date
from datetime import datetime
from datetime import timedelta

import gitlab

from cki_lib.logger import file_logger
from cki_lib.session import get_session
from cki_lib.mailarchive_crawler import MailArchiveCrawler
from cki_lib.pipeline_message import PipelineMsgBase
from cki_lib.misc import get_env_var_or_raise
from rcdefinition import const

from cki.kcidb import objects

from upt_compare.comments import CommentsHelper
from upt_compare import const as compare_const
from upt_compare.dumb_db import DumbDb

LOGGER = file_logger(__name__, dst_file='upt-compare.log')
SESSION = get_session(__name__)


class UptCompare:
    """Compare upt and skt results."""

    def __init__(self):
        """Create an object."""
        self.gitlab_url = get_env_var_or_raise('XCI32_GITLAB_URL')
        self.gitlab_token = get_env_var_or_raise('XCI32_GITLAB_PRIVATE_TOKEN')
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)

        self.comments_helper = CommentsHelper()
        self.comments_helper.change_gitlab_project('cki-project/upt')

    def change_gitlab_project(self, project_id, pipeline_id):
        """Re-create project and pipeline gitlab objects, select given project_id and pipeline_id."""
        self.g_project = self.gitlab.projects.get(project_id)
        self.g_pipeline = objects.GitlabPipeline(self.g_project.pipelines.get(pipeline_id))

    def check_for_artifacts(self, arch):
        """Return True if artifacts for a given pipeline_id/arch/test stage exist."""
        job = self.g_pipeline.get_job('test', arch)
        return getattr(job, 'artifacts_file', None) is not None

    @classmethod
    def load_pipeline_ids(cls):
        """Load pipeline_ids of sent reports for this week."""
        this_weeks_ids = set()

        mhlp = MailArchiveCrawler(const.MAILARCHIVE_SKT_RESULTS, None, const.MAIL_COOKIE_LOC)

        mhlp.update_listarchive(const.MAIL_CACHE_PATH, delete_latest=False,
                                updatelastmonth=True)

        fpath = glob.glob(const.MAIL_CACHE_PATH + '/*')[0]
        messages = PipelineMsgBase.from_file(fpath)

        # remove the existing mbox and create a new empty mbox at its place
        for msg_obj in messages:
            date_cond = datetime.date(msg_obj.tdelivered) > (date.today() - timedelta(days=7))
            if date_cond and 'Test report for' in msg_obj.subject and \
                    'cki-pipeline' in msg_obj.msg.get('X-Gitlab-Path', ''):
                this_weeks_ids.add(msg_obj.pipelineid)

        return this_weeks_ids

    def stage1_update_info(self, project, mr_id, create=False):
        """Load pipeline ids from mailing list and update the db-note in testing PR."""
        # Ensure comments helper has proper project/pr selected, so we can load the note.
        self.comments_helper.change_gitlab_project(project, mr_id)
        db_note = self.comments_helper.get_db_note()
        if not create:
            assert db_note is not None, f"Failed to load data from {project} {mr_id}"
        elif create and not db_note:
            db_note = DumbDb()
            db_note.create_on_gitlab(self.comments_helper.merge_request)

        # Load new pipeline ids from the mailing list.
        new_ids = self.load_pipeline_ids()
        # Ensure we haven't processed these already and that they are not being processed right now.
        new_ids = new_ids.difference(db_note.done)
        new_ids = new_ids.difference(db_note.running)
        db_note.todo = new_ids.union(db_note.todo)
        # Write new data to the gitlab note.
        db_note.update()

        return db_note

    def stage2_run_tests(self, db_note):
        """Run CI tests."""
        # By now, we're reasonably certain we have data to process. If not, we just abort.
        assert db_note.todo is not None and len(db_note.todo) != 0, "No new data to process!"

        # Check how many tests are running. Take up-to 2 pipeline ids from the note and move them to running.
        current_run_count = len(db_note.running)
        how_many_to_start = compare_const.UPT_COMPARE_PARALEL_RUN_COUNT - current_run_count
        # Starting the comparison test should be quick. If it's not, there should be no consequence, as we run only
        # 1 pipeline of upt-compare with pretty big schedule windows.
        i = 0
        pipeline_id_note_id_tuples = set()
        while i < how_many_to_start:
            id2start = db_note.todo.pop()
            db_note.running.add(id2start)
            new_note_id = self.run_comparison_test(id2start)
            if new_note_id:
                pipeline_id_note_id_tuples.add((id2start, new_note_id))
                i += 1
            else:
                db_note.running.remove(id2start)
                db_note.done.add(id2start)
                # Do not increase i, as the test isn't running

        # If we ran any tests, update the db.
        if how_many_to_start > 0:
            db_note.update()

        return pipeline_id_note_id_tuples

    def stage3_wait_for_results(self, pipeline_id_note_id_tuples, wait_seconds=180):
        """Wait for results."""
        matched_results = []

        while pipeline_id_note_id_tuples:
            print(f'* Sleeping for {int(wait_seconds / 60)} minutes...')
            time.sleep(wait_seconds)

            discussions = self.comments_helper.get_discussions()
            tups2remove = set()
            for (pipeline_id, note_id) in pipeline_id_note_id_tuples:
                disc_notes = self.comments_helper.get_disc_notes(discussions, note_id)
                if len(disc_notes) > 1:
                    # There are replies! Check if the bot answered with test results!
                    results = self.comments_helper.get_test_results(disc_notes)
                    if results:
                        matched_results += results
                        tups2remove.add((pipeline_id, note_id))

                        LOGGER.debug('found results')

            if tups2remove:
                pipeline_id_note_id_tuples = pipeline_id_note_id_tuples.difference(tups2remove)

        return matched_results

    def start(self, delay):
        """Load 'dumb-db' comment from a specific merge request, then start processing."""
        # Get new pipeline ids to process and an updated db-note that contains this.
        db_note = self.stage1_update_info(compare_const.UPT_GITLAB_PROJECT, compare_const.UPT_TESTING_MR_ID)

        # If we're not already running more pipelines than intended, move some pipelines from to-do to running.
        pipeline_id_note_id_tuples = self.stage2_run_tests(db_note)

        LOGGER.info('waiting for pipelines...')

        # Wait for pipelines to finish and parse results.
        self.stage3_wait_for_results(pipeline_id_note_id_tuples, wait_seconds=delay)

    def run_comparison_test(self, pipeline_id):
        """Return note id if a test was run."""
        # Ensure cki-project/cki-pipeline is selected
        self.change_gitlab_project(compare_const.CKI_PIPELINE_GITLAB_PROJECT, pipeline_id)

        # NOTE: hack-hack-hack: we check and run tests for x86_64, as this is probably the most plentiful resource we
        # have at our disposal.
        if not self.check_for_artifacts('x86_64'):
            # Test was not run!
            print(f'* Not running tests, artifacts expired for {pipeline_id}!')
            return None

        # Make a comment to cki-ci-bot to actual run
        note_id = self.comments_helper.create_new_run('cki', pipeline_id, arch_override='x86_64')

        # It takes the cki-ci-bot some time to react, so we can't make the check here.
        # Instead, we possibly start another test and wait for the results to show-up.

        return note_id

    def find_finished_results(self, db_note):
        """Search for finished pipeline tests."""
        results = []
        discussions = self.comments_helper.get_discussions()
        # Go through discussions and find
        for pid in db_note.running:
            results.append(self.comments_helper.get_result_for_pipeline(discussions, pid))
        return results

    def remove_broken_comments(self, broken_pipeline_ids, restore):
        """Remove broken testing results from upt testing PR."""
        db_note = self.comments_helper.get_db_note()

        # Find discussions with certain pipeline_ids and delete all their notes.
        discussions = self.comments_helper.get_discussions()
        self.comments_helper.delete_discussions_with_pipeline_ids(discussions, broken_pipeline_ids)

        # Ensure pipelines with broken results aren't in running/finished.
        db_note.running = db_note.running.difference(set(broken_pipeline_ids))
        db_note.done = db_note.done.difference(set(broken_pipeline_ids))

        if restore:
            # Return to to-do, so we can run those again.
            db_note.todo = db_note.todo.union(set(broken_pipeline_ids))

        # Always update the note.
        db_note.update()

    def mark_results(self, pipeline_id, result, set_done):
        """Find results of pipeline comparison testing and mark these with a result."""
        discussions = self.comments_helper.get_discussions()

        self.comments_helper.add_comment_to_discussion(discussions, pipeline_id, result)

        if set_done:
            db_note = self.comments_helper.get_db_note()
            # Move specified pipeline_ids from running/to-do to done.
            db_note.running = db_note.running.difference(set(pipeline_id))
            db_note.todo = db_note.todo.difference(set(pipeline_id))
            db_note.done = db_note.done.union(set(pipeline_id))
            db_note.update()
