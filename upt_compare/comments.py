"""Gitlab comments helper module."""
import re
import gitlab

from cki_lib.misc import get_env_var_or_raise
from rcdefinition.rc_data import DefinitionBase

from upt_compare.const import UPT_TESTING_MR_ID, RGX_BOT_TRIGGER_COMMENT, BOT_COMMENT_RGX
from upt_compare.dumb_db import DumbDb

BOT_RESULTS_MAPPING = {
    ':exclamation:': 'failed',
    ':hourglass_flowing_sand:': 'running',
    ':heavy_check_mark:': 'success',
    ':grey_exclamation:': 'cancelled',
}


class CKICIBotResult(DefinitionBase):
    """Parse cki-ci-bot results."""

    group: str
    branch: str
    pipeline_id: int
    status: str

    def __init__(self, group, branch, pipeline_id, status):
        """Create an object."""
        status = BOT_RESULTS_MAPPING[status]
        super().__init__({'group': group, 'branch': branch, 'pipeline_id': pipeline_id, 'status': status})

    def __str__(self):
        """Return string representation, with status emojis converted using BOT_RESULTS_MAPPING."""
        return f'| {self.group} | {self.branch} | {self.pipeline_id} | {self.status} |'


class CommentsHelper:
    """Manage gitlab comments / discussions / notes."""

    def __init__(self):
        """Create an object."""
        self.gitlab_url = get_env_var_or_raise('GITLAB_URL')
        self.gitlab_token = get_env_var_or_raise('GITLAB_PRIVATE_TOKEN')
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)
        self.merge_request = None
        self.g_project = None

    def reset_db_note(self):
        """Rollback changes; put running pipelines back to to-do, empty running set and update on gitlab."""
        note = self.get_db_note()
        note.running = set()
        note.todo = note.todo.union(note.running)
        note.update()

    def change_gitlab_project(self, project_id, mr_id=UPT_TESTING_MR_ID):
        """Set gitlab project and merge request that the helper class processes."""
        self.g_project = self.gitlab.projects.get(project_id, lazy=True)
        self.merge_request = self.g_project.mergerequests.get(mr_id)

    def get_db_note(self):
        """Scan merge request for first note that matches regex."""
        for note in self.merge_request.notes.list(all=True):
            if 'TODO:' in note.body and 'Running:' in note.body and 'Done:' in note.body:
                return DumbDb.from_note(note)

        return None

    def get_notes(self):
        """Get a list of all notes in the merge request."""
        return list(self.merge_request.notes.list(all=True))

    def get_discussions(self):
        """Return a list of discussions in the specified merge request."""
        return list(self.merge_request.discussions.list(all=True))

    @classmethod
    def get_disc_notes(cls, discussions, note_id):
        """Get notes of a discussion that has note_id note in it."""
        for discussion_item in discussions:
            for note in discussion_item.attributes['notes']:
                if note.get('id', None) == note_id:
                    return list(discussion_item.attributes['notes'])

        return []

    def add_comment_to_discussion(self, discussions, pipeline_ids, comment_body):
        """Add the same comment with comment_body to all discussions that contain pipeline testing results with ids."""
        def add_comment(_, discussion_id, **kwargs):
            discussion = self.merge_request.discussions.get(discussion_id)
            discussion.notes.create({'body': kwargs.get('comment_body')})
            discussion.resolved = True
            discussion.save()

        self._modify_discussions_with_pipeline_ids(add_comment, discussions, pipeline_ids, comment_body=comment_body)

    @classmethod
    def _modify_discussions_with_pipeline_ids(cls, func, discussions, pipeline_ids, **kwargs):
        """Run a method for discussions that have specified pipeline ids."""
        for discussion_item in discussions:
            exec_on_disc_item = False
            notes = discussion_item.attributes['notes']
            for note in notes:
                for pipeline_id in pipeline_ids:
                    # Get text content of the note and check if it has the pipeline_id we want.
                    if re.match(RGX_BOT_TRIGGER_COMMENT.format(pipeline_id=pipeline_id), note.get('body', '')):
                        exec_on_disc_item = True
                        break
                if exec_on_disc_item:
                    break

            if exec_on_disc_item:
                func(notes, discussion_item.attributes['id'], **kwargs)

    def delete_discussions_with_pipeline_ids(self, discussions, pipeline_ids):
        """Remove entire discussions that have specified pipeline ids."""
        def delete_func(notes, discussion_id, **kwargs):
            for note in notes:
                self.merge_request.notes.delete(note.get('id'))

        self._modify_discussions_with_pipeline_ids(delete_func, discussions, pipeline_ids)

    @classmethod
    def get_result_for_pipeline(cls, discussions, pipeline_id):
        """Get cki-ci-bot's comment as parse it into a result for a given pipeline_id."""
        results = []
        for discussion_item in discussions:
            notes = discussion_item.attributes['notes']
            for i, note in enumerate(notes):
                # Get text content of the note and check if it has the pipeline_id we want.
                if not re.match(RGX_BOT_TRIGGER_COMMENT.format(pipeline_id=pipeline_id), note.get('body', '')):
                    continue

                # If so, then next note in the discussion should hold results.
                try:
                    next_note = notes[i + 1]
                except IndexError:
                    pass
                else:
                    body = next_note.get('body', '')
                    for line in body.splitlines():
                        match_result = BOT_COMMENT_RGX.match(line)
                        if match_result and None not in match_result.groups():
                            result = CKICIBotResult(*match_result.groups())
                            results.append(result)

                    return results
        return results

    @staticmethod
    def get_test_results(notes):
        """Parse notes to get a list of CI results."""
        results = []
        for note in notes:
            one_note_result = set()
            for line in note.get('body', '').splitlines():
                match_result = BOT_COMMENT_RGX.match(line)
                if match_result and None not in match_result.groups():
                    res_obj = CKICIBotResult(*match_result.groups())
                    one_note_result.add(res_obj)
            if one_note_result:
                results.append(one_note_result)

        return results

    def create_new_run(self, project_name, pipeline_id, arch_override='x86_64'):
        """Run new testing of a pipeline."""
        # @cki-ci-bot [test][cki/{pipelineid}][skip_beaker=false][arch_override=x86_64][test_mode=only_upt]
        comment_command_start_testing = f'@cki-ci-bot [test][{project_name}/{pipeline_id}][skip_beaker=false]' \
                                        f'[arch_override={arch_override}][test_mode=only_upt]'

        self.merge_request.rebase()

        # This will start testing.
        note = self.merge_request.notes.create({'body': comment_command_start_testing})

        return note.id
