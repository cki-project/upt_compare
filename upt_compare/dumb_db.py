"""Comment helper module to store pipeline ids in a comment."""
import re


class DumbDb:
    """Store pipeline ids to process in a comment."""

    NOTE_RGX = re.compile(r'^Running:\s*([^\n]+)\s*TODO:\s*([^\n]*?)\s*Done:\s*([^\n]*?)\s*$')

    def __init__(self, running=None, todo=None, done=None):
        """Create an object."""
        self.running = set() if not running else running
        self.todo = set() if not todo else todo
        self.done = set() if not done else done

        self.note = None

    @classmethod
    def from_str(cls, str_data):
        """Create an object from string."""
        result = cls.NOTE_RGX.match(str_data)
        if result:
            running, todo, done = result.groups()
            running = set(filter(None, running.split(' ')))
            todo = set(filter(None, todo.split(' ')))
            done = set(filter(None, done.split(' ')))

            dumb_db = DumbDb(running, todo, done)
            return dumb_db

        return None

    @classmethod
    def from_note(cls, note):
        """Create an object from gitlab note."""
        dumb_db = cls.from_str(note.body)
        if dumb_db:
            dumb_db.note = note

        return dumb_db

    def create_on_gitlab(self, merge_request):
        """Create a note on gitlab."""
        self.note = merge_request.notes.create({'body': str(self)})

    def update(self):
        """Update the note on gitlab."""
        self.note.body = str(self)
        self.note.save()

    def __repr__(self):
        """Return "official" string object representation."""
        return self.__str__()

    def __str__(self):
        """Return "informal" string object representation."""
        info = \
            f"""Running: {' '.join(str(x) for x in self.running)}
        TODO: {' '.join(str(x) for x in self.todo)}
        Done: {' '.join(str(x) for x in self.done)}"""

        return info
